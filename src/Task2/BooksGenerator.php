<?php

declare(strict_types=1);

namespace App\Task2;

class BooksGenerator
{
    private int $minPagesNumber;
    private array $libraryBooks;
    private int $maxPrice;
    private array $storeBook;
    private array $filteredBooks = [];

    /**
     * BooksGenerator constructor.
     * @param $minPagesNumber
     * @param $libraryBooks
     * @param $maxPrice
     * @param $storeBook
     */
    public function __construct(int $minPagesNumber, array $libraryBooks, int $maxPrice, array $storeBook)
    {
        $this->minPagesNumber = $minPagesNumber;
        $this->libraryBooks = $libraryBooks;
        $this->maxPrice = $maxPrice;
        $this->storeBook = $storeBook;
    }

    private function libraryFilter(): void
    {
        foreach ($this->libraryBooks as $libValue) {
            if ($libValue->getPagesNumber() >= $this->minPagesNumber) {
                $this->filteredBooks[] = $libValue;
            }
        }
    }

    private function storeFilter(): void
    {
        foreach ($this->storeBook as $storeValue) {
            if ($storeValue->getPrice() <= $this->maxPrice) {
                $this->filteredBooks[] = $storeValue;
            }
        }
    }


    public function generate(): \Generator
    {
        $this->libraryFilter();
        $this->storeFilter();
        foreach ($this->filteredBooks as $bookItem) {
            yield $bookItem;
        }

    }
}