<?php

declare(strict_types=1);

namespace App\Task1;

class Track
{
    public array $ontrack = [];
    public float $lapLength;
    public int $lapsNumber;

    /**
     * Track constructor.
     * @param float $lapLength
     * @param int $lapsNumber
     */
    public function __construct(float $lapLength, int $lapsNumber)
    {
        $this->lapLength = $lapLength;
        $this->lapsNumber = $lapsNumber;
    }

    public function getLapLength(): float
    {
        return $this->lapLength;
    }

    public function getLapsNumber(): int
    {
        return $this->lapsNumber;
    }

    public function add(Car $car): void
    {
        array_push($this->ontrack, $car);
    }

    public function all(): array
    {
        return $this->ontrack;
    }


    public function pathLength(): float
    {
        return $this->getLapLength() * $this->getLapsNumber();
    }


    public function run(): Car
    {
        $allCar = $this->all();
        $minVal = null;
        if (count($allCar) > 0) {
            foreach ($allCar as $carItem) {
                $pathTime = round($this->pathLength() / $carItem->speed, 2) * 60;
                $pitstopCount = floor($this->pathLength() / $carItem->getFuelConsumptionForDistance());

                if ($pitstopCount >= 1) {
                    $pathTime += $carItem->pitStopTime * $pitstopCount;
                }
                if ($pathTime < $minVal || $minVal === null) {
                    $minVal = $pathTime;
                    $winCar = $carItem;
                }
            }
            return $winCar;
        } else {
            throw new \LogicException('Empty array.Add cars!');
        }

    }


}